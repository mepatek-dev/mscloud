<?php
declare(strict_types=1);

namespace Mepatek\MSCloud\Model;

use GuzzleHttp\Psr7\Stream;
use http\Encoding\Stream\Inflate;
use Microsoft\Graph\Http\GraphResponse;
use Microsoft\Graph\Model\DriveItem;
use Nette\Caching\Cache;
use Mepatek\UserManager\AuthDrivers\AzureOAuth2AuthDriver;
use Microsoft\Graph\Model\Drive;
use Microsoft\Graph\Model\Site;
use Nette\Caching\IStorage;
use Nette\Security\User;

/**
 * Class FilesFacade
 * Facade for MS Graph for sites, drives and sites
 *
 * @package Mepatek\MSCloud\Model
 */
class FilesFacade extends GraphFacade
{
    /** @var string */
    private $tempDir;

    /**
     * TeamsFacade constructor.
     * @param string $tempDir
     * @param AzureOAuth2AuthDriver $azureOAuth2AuthDriver
     * @param IStorage $storage
     * @param User $user
     * @param string $expiration
     */
    public function __construct(
        string                $temDir,
        AzureOAuth2AuthDriver $azureOAuth2AuthDriver,
        IStorage              $storage,
        User                  $user,
        string                $expiration = null
    )
    {
        $this->tempDir = $temDir;
        parent::__construct($azureOAuth2AuthDriver, $storage, $user, $expiration);
    }

    /**
     * Get List of sites
     * https://docs.microsoft.com/en-us/graph/api/site-search?view=graph-rest-1.0
     *
     * @param bool|null $refresh
     * @return Site[]
     * @throws \Microsoft\Graph\Exception\GraphException
     * @throws \Throwable
     */
    public function listSites(?bool $refresh = false): array
    {
        $sites = [];
        if (!$refresh) {
            $sites = $this->getCache()->load("sites");
        }
        if ($sites === null) {
            $sites = $this->getGraph()
                ->createRequest("GET", "/sites?search=*")
                ->setReturnType(Site::class)
                ->execute();
            $this->getCache()->save(
                "joinedTeams",
                $sites,
                [
                    Cache::EXPIRE => $this->expiration,
                ]
            );
        }
        return $sites;
    }

    /**
     * Get default drive for site
     * https://docs.microsoft.com/en-us/graph/api/drive-get?view=graph-rest-1.0
     *
     * @param string $siteId
     * @return Drive|null
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getSiteDefaultDrive(string $siteId): ?Drive
    {
        $drive = $this->getGraph()
            ->createRequest("GET", "/sites/" . $siteId . "/drive")
            ->setReturnType(Drive::class)
            ->execute();
        return $drive;
    }

    /**
     * Get DriveItems for drive or drive and item
     * https://docs.microsoft.com/en-us/graph/api/driveitem-list-children?view=graph-rest-1.0
     *
     * @param string $driveId
     * @param string $itemId
     * @param bool|null $thumbnails
     * @return DriveItem[]
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function listDriveItems(
        string  $driveId,
        ?string $itemId = null,
        ?bool   $thumbnails = false
    )
    {
        if ($itemId) {
            $endpoint = "/drives/" . $driveId . "/items/" . $itemId . "/children";
//            $endpoint = "/drive/items/" . $itemId . "/children";
        } else {
            $endpoint = "/drives/" . $driveId . "/root/children";
        }
        bdump($endpoint);
        $endpoint .= $thumbnails ? "?\$expand=thumbnails" : "";
        $files = $this->getGraph()
            ->createCollectionRequest("GET", $endpoint)
            ->setReturnType(DriveItem::class)
            ->execute();
        bdump($files);
        return $files;
    }

    /**
     * @param string $itemId
     * @return null|DriveItem
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getDriveItem(
        string $driveId,
        string $itemId
    ): ?DriveItem
    {
        $endpoint = "/drives/" . $driveId . "/items/" . $itemId;
        /** @var DriveItem $driveItem */
        $driveItem = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->setReturnType(DriveItem::class)
            ->execute();
        return $driveItem;
    }

    /**
     * @param string $driveId
     * @param string $itemId
     * @return string
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getFileContent(
        string $driveId,
        string $itemId
    )
    {
        $fileName = $this->getTmpFileName();
        $this->downloadFileContent($driveId, $itemId, $fileName);
        $content = file_get_contents($fileName);
        @unlink($fileName);
        return $content;
    }

    public function getTmpFileName(): string
    {
        return tempnam($this->tempDir, "fileFacade_");
    }

    /**
     * @param string $driveId
     * @param string $folderItemId
     * @param string $filename
     * @param string $filenameContent
     * @return DriveItem
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function createFile(
        string $driveId,
        string $folderItemId,
        string $filename,
        string $filenameContent
    ): DriveItem {
        $endpoint = "/drives/" . $driveId . "/items/" . $folderItemId . ":/" . $filename . ":/content";
        /** @var GraphResponse $response */
        $response = $this->getGraph()
            ->createRequest("PUT", $endpoint)
            ->setReturnType(DriveItem::class)
            ->upload($filenameContent);
        return $response;
    }

    /**
     * @param string $driveId
     * @param string $itemId
     * @param string $fileName
     * @param bool $inPdf
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function downloadFileContent(
        string $driveId,
        string $itemId,
        string $fileName,
        bool $inPdf = false
    ) {
        $endpoint = "/drives/" . $driveId . "/items/" . $itemId . "/content";
        if ($inPdf) {
            $endpoint .= "?format=pdf";
        }
        /** @var Stream $file */
        $file = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->download($fileName);
//        var_dump($file);
    }

    /**
     * @param string $driveId
     * @param string $itemId
     * @param int|null $page
     * @param int|null $zoom
     * @return string
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getFilePreviewUrl(
        string $driveId,
        string $itemId,
        ?int $page = null,
        ?int $zoom = null
    ): string {
        $endpoint = "/drives/" . $driveId . "/items/" . $itemId . "/preview";
        /** @var Stream $response */
        $response = $this->getGraph()
            ->createRequest("POST", $endpoint)
            ->execute();
        return $response->getBody()["getUrl"];
    }
}
