<?php
declare(strict_types=1);

namespace Mepatek\MSCloud\Model;

use Mepatek\UserManager\AuthDrivers\AzureOAuth2AuthDriver;
use Microsoft\Graph\Model\Attachment;
use Microsoft\Graph\Model\EmailAddress;
use Microsoft\Graph\Model\ItemBody;
use Microsoft\Graph\Model\Recipient;
use Nette\Caching\IStorage;
use Nette\Mail\Mailer;
use Nette\Mail\Message;
use Nette\Security\User;
use Nette\Utils\Strings;

class MailFacade extends GraphFacade
{
    private ?string $fromEmail;
    private ?string $sendAsUserId;

    /**
     * @param AzureOAuth2AuthDriver $azureOAuth2AuthDriver
     * @param IStorage $storage
     * @param User $user
     * @param string $expiration
     */
    public function __construct(
        AzureOAuth2AuthDriver $azureOAuth2AuthDriver,
        IStorage              $storage,
        User                  $user,
        string                $expiration = null
    )
    {
        parent::__construct($azureOAuth2AuthDriver, $storage, $user, $expiration);
    }


    function send(Message $message): void
    {
        $msMsg = new \Microsoft\Graph\Model\Message();
        if ($this->fromEmail) {
            $msMsg->setFrom((new Recipient())->setEmailAddress((new EmailAddress())->setAddress($this->fromEmail)));
        } else {
            // From
            foreach ($message->getHeader("From") as $e => $n) {
                $msMsg->setFrom((new Recipient())->setEmailAddress((new EmailAddress())->setAddress($e)));
            }
        }
        // ReplyTo
        if ($message->getHeader("Reply-To")) {
            $recipients = [];
            $headers = $message->getHeader("Reply-To") ?: [];
            foreach ($headers as $e => $n) {
                $recipients[] = (new Recipient())->setEmailAddress((new EmailAddress)->setAddress($e)->setName($n));
            }
            $msMsg->setReplyTo($recipients);
        }

        $msMsg->setSubject($message->getSubject());
        $msMsg->setBody((new ItemBody())
            ->setContentType("html")
            ->setContent($message->getHtmlBody()));

        // To
        if ($message->getHeader("To")) {
            $recipients = [];
            foreach ($message->getHeader("To") as $e => $n) {
                $recipients[] = (new Recipient())->setEmailAddress((new EmailAddress)->setAddress($e)->setName($n));
            }
            $msMsg->setToRecipients($recipients);
        }
        // Cc
        if ($message->getHeader("Cc")) {
            $recipients = [];
            $headers = $message->getHeader("Cc") ?: [];
            foreach ($headers as $e => $n) {
                $recipients[] = (new Recipient())->setEmailAddress((new EmailAddress)->setAddress($e)->setName($n));
            }
            $msMsg->setCcRecipients($recipients);
        }
        // Bcc
        if ($message->getHeader("Bcc")) {
            $recipients = [];
            $headers = $message->getHeader("Bcc") ?: [];
            foreach ($headers as $e => $n) {
                $recipients[] = (new Recipient())->setEmailAddress((new EmailAddress)->setAddress($e)->setName($n));
            }
            $msMsg->setBccRecipients($recipients);
        }

        $attachemnts = [];
        foreach ($message->getAttachments() as $part) {
            $fdm = Strings::match($part->getHeader("Content-Disposition"), '~(.+)\;\sfilename=\"(.+)\"~');

            $filename = $fdm[2];
            $attachemnts[] = (object)
                [
                    "@odata.type" => "#microsoft.graph.fileAttachment",
                    "name" => $filename,
                    "contentType" => $part->getHeader("Content-Type"),
                    "contentBytes" => base64_encode($part->getBody()),
                    "isInline" => $fdm[2]==="inline",
                ];
        }
        if (count($attachemnts)>0) {
            $msMsg->setAttachments($attachemnts);
        }

        $oldUAT = $this->isUseAppToken();
        $this->setUseAppToken(true);
        bdump($msMsg);
        $response = $this->getGraph()
//            ->createRequest("POST", "/users//sendMail")
            ->createRequest("POST", "/users/" . urlencode($this->getSendAsUserId()) . " /sendMail")
            ->attachBody(["message" => $msMsg, "saveToSentItems" => false])
//                ->createRequest("GET", "/groups")
            ->execute();
        $response->getBody();
        $this->setUseAppToken($oldUAT);
    }

    /**
     * @return string
     */
    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail(string $fromEmail): void
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string
     */
    public function getSendAsUserId(): string
    {
        return $this->sendAsUserId;
    }

    /**
     * @param string $sendAsUserId
     */
    public function setSendAsUserId(string $sendAsUserId): void
    {
        $this->sendAsUserId = $sendAsUserId;
    }
}