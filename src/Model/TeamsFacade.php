<?php
declare(strict_types=1);

namespace Mepatek\MSCloud\Model;

use Mepatek\UserManager\AuthDrivers\AzureOAuth2AuthDriver;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Channel;
use Microsoft\Graph\Model\DriveItem;
use Microsoft\Graph\Model\Group;
use Microsoft\Graph\Model\Team;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Security\User;

class TeamsFacade extends GraphFacade
{
    /** @var Graph */
    private $graph;
    /** @var IStorage */
    private $storage;
    /** @var Cache */
    private $cache = null;
    /** @var string */
    protected $expiration;
    /** @var User */
    private $user;

    /**
     * TeamsFacade constructor.
     * @param AzureOAuth2AuthDriver $azureOAuth2AuthDriver
     * @param IStorage $storage
     * @param User $user
     * @param string $expiration
     */
    public function __construct(
        AzureOAuth2AuthDriver $azureOAuth2AuthDriver,
        IStorage              $storage,
        User                  $user,
        string                $expiration = null
    ) {
        parent::__construct($azureOAuth2AuthDriver, $storage, $user, $expiration);
    }


    /**
     * @param bool $refresh
     * @return Group[]
     * @throws \Microsoft\Graph\Exception\GraphException
     * @throws \Throwable
     */
    public function listAllTeams($refresh = false): array
    {
        $allTeams = null;
        if (!$refresh) {
            $allTeams = $this->getCache()->load("allTeams");
        }

        if ($allTeams === null) {
            $allTeams = $this->getGraph()
                ->setApiVersion("beta")
                ->createRequest("GET", "/groups?\$filter=resourceProvisioningOptions/Any(x:x eq 'Team')")
//                ->createRequest("GET", "/groups")
                ->setReturnType(Group::class)
                ->execute();

            $this->getCache()->save(
                "allTeams",
                $allTeams,
                [
                    Cache::EXPIRE => $this->expiration,
                ]
            );
        }
        return $allTeams;
    }

    /**
     * @param string $teamId
     * @return Team|null
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getTeam(string $teamId): ?Team
    {
        $endpoint = "/teams/" . $teamId;
        $team = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->setReturnType(Team::class)
            ->execute();
        return $team;
    }

    /**
     * @param string $teamId
     * @return Channel[]
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getTeamChannels($team): array
    {
        if (is_string($team)) {
            $teamId = $team;
        } else if ($team instanceof Team) {
            $teamId = $team->getId();
        } else {
            throw new \Mepatek\MSCloud\Exception\BadParametersException("Bad $teams parameter. Must be ID (string) or Team object");
        }

        $endpoint = "/teams/" . $teamId . "/channels";

        $channels = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->setReturnType(Channel::class)
            ->execute();

        return $channels;
    }

    /**
     * @param string $teamId
     * @return Channel|null
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getTeamChannel($team, string $channelId): ?Channel
    {
        if (is_string($team)) {
            $teamId = $team;
        } else if ($team instanceof Team) {
            $teamId = $team->getId();
        } else {
            throw new \Mepatek\MSCloud\Exception\BadParametersException("Bad $teams parameter. Must be ID (string) or Team object");
        }

        $endpoint = "/teams/" . $teamId . "/channels/" . $channelId;

        $channel = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->setReturnType(Channel::class)
            ->execute();

        return $channel;
    }

    /**
     * @param string $teamId
     * @return DriveItem|null
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getTeamChannelFilesFolder($team, string $channelId): ?DriveItem
    {
        if (is_string($team)) {
            $teamId = $team;
        } else if ($team instanceof Team) {
            $teamId = $team->getId();
        } else {
            throw new \Mepatek\MSCloud\Exception\BadParametersException("Bad $teams parameter. Must be ID (string) or Team object");
        }

        $endpoint = "/teams/" . $teamId . "/channels/" . $channelId . "/filesFolder";

        $filesFolder = $this->getGraph()
            ->createRequest("GET", $endpoint)
            ->setReturnType(DriveItem::class)
            ->execute();

        return $filesFolder;
    }

    /**
     * @param bool $refresh
     * @return Team[]
     * @throws \Microsoft\Graph\Exception\GraphException
     * @throws \Throwable
     */
    public function listJoinedTeams($refresh = false): array
    {
        $joinedTeams = null;
        if (!$refresh) {
            $joinedTeams = $this->getCache()->load("joinedTeams");
        }

        if ($joinedTeams === null) {
            $joinedTeams = $this->getGraph()
                ->createRequest("GET", "/me/joinedTeams")
                ->setReturnType(Team::class)
                ->execute();

            $this->getCache()->save(
                "joinedTeams",
                $joinedTeams,
                [
                    Cache::EXPIRE => $this->expiration,
                ]
            );
        }
        return $joinedTeams;
    }
}
