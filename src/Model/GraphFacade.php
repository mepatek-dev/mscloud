<?php
declare(strict_types=1);

namespace Mepatek\MSCloud\Model;

use Nette\Caching\Cache;
use Mepatek\UserManager\AuthDrivers\AzureOAuth2AuthDriver;
use Microsoft\Graph\Graph;
use Nette\Caching\IStorage;
use Nette\Security\User;

abstract class GraphFacade
{
    /** @var Graph */
    private $graph = null;
    /** @var AzureOAuth2AuthDriver */
    private $azureOAuth2AuthDriver;
    /** @var IStorage */
    private $storage;
    /** @var Cache */
    private $cache = null;
    /** @var string */
    protected $expiration;
    /** @var bool */
    private $useAppToken = false;
    /** @var User */
    private $user;

    /**
     * TeamsFacade constructor.
     * @param AzureOAuth2AuthDriver $azureOAuth2AuthDriver
     * @param IStorage $storage
     * @param User $user
     * @param string $expiration
     */
    public function __construct(
        AzureOAuth2AuthDriver $azureOAuth2AuthDriver,
        IStorage $storage,
        User $user,
        ?string $expiration
    ) {

        $this->azureOAuth2AuthDriver = $azureOAuth2AuthDriver;
        $this->storage = $storage;
        $this->expiration = $expiration ?: "20 minutes";
        $this->user = $user;
    }

    /**
     * @return Cache
     */
    protected function getCache(): Cache
    {
        if ($this->cache === null) {
            $this->cache = new Cache(
                $this->storage,
                "mscloud-files-" . $this->user->getId()
            );
        }
        return $this->cache;
    }

    /**
     * @return bool
     */
    public function isUseAppToken(): bool
    {
        return $this->useAppToken;
    }

    /**
     * @param bool $useAppToken
     */
    public function setUseAppToken(bool $useAppToken): void
    {
        $this->useAppToken = $useAppToken;
        $this->graph = null;
    }

    /**
     * @return Graph
     */
    protected function getGraph(): Graph
    {
        if ($this->graph == null) {
            if ($this->useAppToken) {
                $accessToken = $this->azureOAuth2AuthDriver->getAppToken();
                $this->graph = $this->azureOAuth2AuthDriver->getGraph($accessToken);
            } else {
                $this->graph = $this->azureOAuth2AuthDriver->getGraph();
            }
        }
        return $this->graph;
    }

}
